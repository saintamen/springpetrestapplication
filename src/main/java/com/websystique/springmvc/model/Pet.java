package com.websystique.springmvc.model;

public class Pet {
    private int id;
    private String name;
    private String owner;
    private int age;
    private double height;
    private double weight;
    private boolean pureBred;

    public Pet() {
        this.id = 0;
    }

    public Pet(int id, String name, String owner, int age, double height, double weight, boolean pureBred) {
        this.id = id;
        this.name = name;
        this.owner = owner;
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.pureBred = pureBred;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public boolean isPureBred() {
        return pureBred;
    }

    public void setPureBred(boolean pureBred) {
        this.pureBred = pureBred;
    }
}

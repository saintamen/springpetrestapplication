package com.websystique.springmvc.controller;

import com.websystique.springmvc.model.Pet;
import com.websystique.springmvc.model.User;
import com.websystique.springmvc.service.PetService;
import com.websystique.springmvc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@RestController
public class PetRestController {

    @Autowired
    PetService petService;

    @RequestMapping(value = "/pets/", method = RequestMethod.GET)
    public ResponseEntity<List<Pet>> listAllPets() {
        List<Pet> pets = petService.findAllPets();
        if(pets.isEmpty()){
            return new ResponseEntity<List<Pet>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Pet>>(pets, HttpStatus.OK);
    }

    @RequestMapping(value = "/pets/", method = RequestMethod.POST)
    public ResponseEntity<Void> createPet(@RequestBody Pet pet, UriComponentsBuilder ucBuilder) {
        System.out.println("Creating User " + pet.getName());

        if (petService.isPetExist(pet)) {
            System.out.println("A Pet with name " + pet.getName() + " already exist");
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }

        petService.savePet(pet);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/pets/{id}").buildAndExpand(pet.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
}

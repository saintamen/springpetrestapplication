package com.websystique.springmvc.service;

import com.websystique.springmvc.model.Pet;
import com.websystique.springmvc.model.User;

import java.util.List;


public interface PetService {
	
	Pet findById(long id);

	Pet findByName(String name);
	
	void savePet(Pet pet);
	
	void deleteUserById(long id);

	List<Pet> findAllPets();
	
	void deleteAllPets();
	
	public boolean isPetExist(Pet pet);
	
}

package com.websystique.springmvc.service;

import com.websystique.springmvc.model.Pet;
import com.websystique.springmvc.model.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service("petService")
public class PetServiceImpl implements PetService {
    private static List<Pet> pets = new ArrayList<>();
    private static int counter = 1;

    static {
        Logger.getLogger(PetServiceImpl.class.getName())
                .log(Level.INFO, "Populating pets.");
        pets.addAll(populateDummyUsers());
    }

    private static List<Pet> populateDummyUsers() {
        List<Pet> pets = new ArrayList<Pet>();
        pets.add(new Pet(counter++,"Mruczek", "Pablo", 12, 10.3, 10.3, true));
        pets.add(new Pet(counter++,"Puszek", "Ania", 2, 13.0, 0.3, false));
        pets.add(new Pet(counter++,"Okruszek", "Rafał", 13, 0.3, 1.3, true));
        return pets;
    }

    @Override
    public Pet findById(long id) {
        try {
            return pets.stream().
                    filter(pet -> pet.getId() == id).
                    collect(Collectors.toList())
                    .get(0);
        } catch (ArrayIndexOutOfBoundsException aioobe) {
            return null;
        }
//		for (Pet pet :pets) {
//			if(pet.getId() == id){
//				return pet;
//			}
//		}
//		return null;
    }

    @Override
    public Pet findByName(String name) {
        for (Pet pet : pets) {
            if (pet.getName().equals(name)) {
                return pet;
            }
        }
        return null;
    }

    @Override
    public void savePet(Pet pet) {
        pet.setId(counter++);
        Logger.getLogger(getClass().getName())
                .log(Level.INFO, "Saving pet with id:" + pet.getId());
        pets.add(pet);
    }

    @Override
    public void deleteUserById(long id) {
        for (Pet pet : pets) {
            if (pet.getId() == id) {
                pets.remove(pet);
                Logger.getLogger(getClass().getName())
                        .log(Level.INFO, "Removed pet with id:" + id);
                // po znalezieniu mozemy zakonczyc metode
                return;
            }
        }
        Logger.getLogger(getClass().getName())
                .log(Level.WARNING, "Unable to delete pet - not existing");
    }

    @Override
    public List<Pet> findAllPets() {
        Logger.getLogger(getClass().getName())
                .log(Level.INFO, "Getting all pets");
        return pets;
    }

    @Override
    public void deleteAllPets() {
        Logger.getLogger(getClass().getName())
                .log(Level.INFO, "Deleting all pets");
        pets.clear();
    }

    @Override
    public boolean isPetExist(Pet pet) {
        Logger.getLogger(getClass().getName())
                .log(Level.INFO, "Finding pet: " + pet);
        return pets.contains(pet);
    }


}

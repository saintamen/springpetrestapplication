'use strict';

angular.module('myApp').factory('PetsService', ['$http', '$q', function($http, $q){

    var REST_SERVICE_URI = 'http://localhost:8080/pets/';

    var factory = {
        fetchAllPets: fetchAllPets,
        createPet: createPet,
        // deleteUser:deleteUser
    };

    return factory;

    function fetchAllPets() {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function(errResponse){
                    console.error('Error while fetching Pets');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function createPet(pet) {
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URI, pet)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function(errResponse){
                    console.error('Error while creating Pet');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

}]);

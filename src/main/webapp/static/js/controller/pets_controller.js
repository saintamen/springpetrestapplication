'use strict';

angular.module('myApp').controller('PetsController', ['$scope', 'PetsService',
    function ($scope, PetsService) {
    var self = this;
    self.pet = {id: null, name: '', owner: '', age: 0, height: 0.0, width: 0.0, pureBred: false};
    self.pets = [];

    self.submit = submit;
    self.reset = reset;

    fetchAllPets();

    function fetchAllPets() {
        console.log('Fetching pets');
        PetsService.fetchAllPets()
            .then(
                function (d) {
                    self.pets = d;
                },
                function (errResponse) {
                    console.error('Error while fetching Users');
                }
            );
    }

    function createPet(pet) {
        console.log('Pet creating');
        PetsService.createPet(pet)
            .then(
                function (errResponse) {
                    console.error('Error while creating User');
                }
            );
    }

    //
    // function deleteUser(id){
    //     UserService.deleteUser(id)
    //         .then(
    //         fetchAllUsers,
    //         function(errResponse){
    //             console.error('Error while deleting User');
    //         }
    //     );
    // }

    function submit() {
        console.log('Saving New Pet', self.pet);
        createPet(self.pet);
        reset();
    }

    //
    // function remove(id){
    //     console.log('id to be deleted', id);
    //     if(self.user.id === id) {//clean form if the user to be deleted is shown there.
    //         reset();
    //     }
    //     deleteUser(id);
    // }


    function reset() {
        self.user = {id: null, username: '', address: '', email: ''};
        $scope.myForm.$setPristine(); //reset Form
    }

}]);
